import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { AffichComponent } from "./mep/affich/affich.component";
import { InboxComponent } from "./messaging/inbox/inbox.component";
import { ListMessageComponent } from "./messaging/list-message/list-message.component";
import { NotifComponent } from "./messaging/notif/notif.component";
import { WrittingComponent } from "./messaging/writting/writting.component";
import { AffichageComponent } from "./pages/affichage/affichage.component";
import { ChatboxComponent } from "./pages/chatbox/chatbox.component";
import { DashboardComponent } from "./pages/dashboard/dashboard.component";


@NgModule({
  imports : [

    RouterModule.forRoot([

      { path: 'notif', component: NotifComponent },
      { path: 'inbox', component: InboxComponent },
      { path: 'chatbox', component: ChatboxComponent },
      { path: 'dashboard', component: DashboardComponent},
      { path: 'project', component: AffichageComponent },
      { path: 'writting', component: WrittingComponent },
      { path: 'writting', component: ListMessageComponent },
      { path: 'affich', component: AffichComponent },
      { path: 'list-message', component: ListMessageComponent }




    ])

  ] ,
  exports : [
    RouterModule
  ]
})
export class RoutingModule {

}
