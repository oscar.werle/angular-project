import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { filter } from 'rxjs/operators';
import { Message } from '../inbox/inbox.component';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class DataMessageService {





  private subject = new BehaviorSubject<Message[]>([]);

  constructor() {

    this.subject.next([{

      title : ' Titre du message',
      content : 'contenus du message',
      isRead : false ,
      sent : new Date()
    }])

   }

  private messages : Message[] = [

    {
      title : ' Titre du message',
      content : 'contenus du message',
      isRead : false ,
      sent : new Date()
    }
  ]


// fonction qui retourne mon messages
  getMessages(): Message[]{
 return  this.messages
}

// fonction qui push mes message dans ma liste de messages
addMessage(message: Message){
  // j'extrait mes messages et fais une copie des données
  const msgs = this.subject.getValue();
  msgs.push(message);
  this.subject.next(msgs);
  console.log(this.messages)
}

// Transforme la liste de message en Observable de messages

getObservableMess(): Observable<Message[]>{
  return of(this.messages)
}

// ici je ne renvoie que sa partie observable
getSubjectMessages(): Observable<Message[]>{
  return this.subject.asObservable();
}

getUnreadMessageAsObservable(): Observable<Message[]>{
return this.subject
.pipe(
  map( msgs => msgs.filter(m => !m.isRead))
);
}

}
