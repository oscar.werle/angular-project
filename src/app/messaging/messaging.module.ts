import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InboxComponent } from './inbox/inbox.component';
import { NotifComponent } from './notif/notif.component';
import { WrittingComponent } from './writting/writting.component';
import { FormsModule } from '@angular/forms';
import { ListMessageComponent } from './list-message/list-message.component';
import { MepModule } from '../mep/mep.module';



@NgModule({
  declarations: [
    InboxComponent,
    NotifComponent,
    WrittingComponent,
    ListMessageComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    MepModule
  ],
  exports : [
    InboxComponent,
    NotifComponent,
    WrittingComponent,
    ListMessageComponent
  ]
})
export class MessagingModule { }
