import { Component, OnInit } from '@angular/core';
import { DataMessageService } from '../service/data-message.service';
import { Message } from '../inbox/inbox.component';


@Component({
  selector: 'app-writting',
  templateUrl: './writting.component.html',
  styleUrls: ['./writting.component.scss']
})
export class WrittingComponent implements OnInit {


 m: Message;
 private messages : Message[] = [];
  constructor(private myService: DataMessageService) {
  }



  ngOnInit(): void {
    this.m = {
      content : 'content' ,
      title : 'title',
      sent : new Date(),
      isRead : false
    }
  }

  addNewMessage() {
    this.myService.addMessage(this.m);

  }

}

