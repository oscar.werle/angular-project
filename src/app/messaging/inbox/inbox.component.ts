
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { DataMessageService } from '../service/data-message.service';
import { WrittingComponent } from '../writting/writting.component';



export interface Message{

  content : string ;
  title : string ;
  sent : Date ;
  isRead : boolean ;
  }



@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  title: string = 'mon super titre';


  messages: Message[] = [];


  constructor(private myService: DataMessageService) { }

    // private messages : Message[] = [

      // {
      //   title : ' Titre du message',
      //   content : 'contenus du message',
      //   isRead : false ,
      //   sent : new Date()
      // },
      // {
      //   title : ' Titre du message',
      //   content : 'contenus du message',
      //   isRead : false ,
      //   sent : new Date()
      // }
    // ];


  ngOnInit(): void {
// synchrone
    this.messages = this.myService.getMessages();


    // partie asynchrone ( les méthode font leur traitements de leur "coté")
    // je souscris a l'obervable pour mes traitement
    // subscribe demande une fonction de rappel ( callback)
    this.myService.getObservableMess().subscribe((datas)=>{
      this.messages = datas ;
    })
      console.log(this.messages);
    }


    change(){
      this.title = 'titre changé après le clique'
    }
  }

//  exemple de callback
// racourci execute ((result)=>
//  console.log(result))
  execute(function (result) {
    console.log(result);
  })


  function execute (callback){
    // traitement
    let valeur = 42;
    callback(valeur);
  }
  // change(){
  //   this.message = =
  // }

// }

asynchronus((datas)=>{
  console.log(datas);
})
// traitement du main

function asynchronus(callback){
// traitement 20min
let valeur = 42;
callback(valeur);
}

//  un traitement asynchrone a besoin d'une fonction de rapel
