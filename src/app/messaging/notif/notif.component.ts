import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { DataMessageService } from '../service/data-message.service';

@Component({
  selector: 'app-notif',
  templateUrl: './notif.component.html',
  styleUrls: ['./notif.component.scss']
})
export class NotifComponent implements OnInit {

  notReadMessages: number;

  constructor(private service: DataMessageService) { }
// affichages des notifs
  ngOnInit(): void {

    //  je fais mes traitement locaux
    this.service
    //on récupère l'observable
    .getSubjectMessages()
    // pipe permet des opération intermédiaire de modification de datas
    .pipe(
      map((msgs, idx ) =>{
        // je return un nombre
        return msgs.filter((m)=>{
          return !m.isRead;
        }).length
      })
     )


    // data représente ma liste de messages
    .subscribe( datas => this.notReadMessages =  unrad)

  }




}
