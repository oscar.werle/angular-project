import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { MessagingModule } from './messaging/messaging.module';
import { RoutingModule } from './app.routing.module';
import { PagesModule } from './pages/pages.module';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms' ;
import { MepModule } from './mep/mep.module';

@NgModule ({
  declarations : [
     AppComponent
  ],
  imports: [
    BrowserModule,
    RoutingModule,
    RouterModule,
    PagesModule,
    MessagingModule,
    FormsModule,
    MepModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
