import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-right',
  templateUrl: './right.component.html',
  styleUrls: ['./right.component.scss']
})
export class RightComponent implements OnInit {

  constructor(public data: DataService) { }

  artist: string[] = [];

  // lui dis de passer par notre service pour chercher notre liste
  ngOnInit(): void {
    this.artist = this.data.artist;
  }

}
