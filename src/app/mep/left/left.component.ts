import { Component, OnInit } from '@angular/core';
import { DataService } from '../service/data.service';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.scss']
})
export class LeftComponent implements OnInit {
    artist = '';

  constructor(private data: DataService) { }

  ngOnInit(): void {
  }

  //  ma fonction add me permet de recupéré les data de mon form et de les envoyer
  // dans la liste de mon service
  addArtist(): void{
    console.log(this.artist);
    this.data.artist.push(this.artist);

  }

}
