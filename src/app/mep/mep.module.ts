import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftComponent } from './left/left.component';
import { RightComponent } from './right/right.component';
import { AffichComponent } from './affich/affich.component';
import { FormsModule } from '@angular/forms';



@NgModule({
  declarations: [
    LeftComponent,
    RightComponent,
    AffichComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports : [
    LeftComponent,
    RightComponent,
    AffichComponent
  ]
})
export class MepModule { }
