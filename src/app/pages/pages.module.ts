import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AffichageComponent } from './affichage/affichage.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ProjectComponent } from './project/project.component';
import { MessagingModule } from '../messaging/messaging.module';
import { ChatboxComponent } from './chatbox/chatbox.component';
import {CardModule} from 'primeng/card';
import {TabViewModule} from 'primeng/tabview';
import { HeadComponent } from './head/head.component';




@NgModule({
  declarations: [
    AffichageComponent,
    DashboardComponent,
    ProjectComponent,
    ChatboxComponent,
    HeadComponent
  ],
  imports: [
    CommonModule,
    MessagingModule,
    CardModule,
    TabViewModule
  ],
exports : [
  AffichageComponent,
  DashboardComponent,
  ProjectComponent,
  ChatboxComponent,
  MessagingModule,
  HeadComponent
]
})
export class PagesModule { }
